class LeadsPage < AbstractPage
    include PageObject
    include RelatedToPicker
    
    page_url "https://app.futuresimple.com/leads"
    
    link(:create_lead, :id => "leads-new")
    # div(:lead-li, :class => "lead-left clickable")
    
    # def lead_anchor
    #      
    # end
    
    def add_lead
        create_lead_element.when_visible.href
    end
    
end
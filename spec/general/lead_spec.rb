require 'spec_helper'

def generate_lead
    lead = [Faker::Name.first_name,
        Faker::Name.last_name,
        Faker::Company.name,
        Faker::Name.title,
        Faker::Internet.email,
        Faker::PhoneNumber.cell_phone,
        Faker::Address.street_address,
        Faker::Address.city,
        Faker::Address.zip,
        Faker::Address.state]
end

def generate_note
    note = Faker::Lorem.sentence
end

shared_examples "Add a lead and a note" do |lead, note|
    it "fills out new lead's details and saves them" do
        on(NewLeadPage).first_name=lead[0]
        on(NewLeadPage).last_name=lead[1]
        on(NewLeadPage).company=lead[2]
        on(NewLeadPage).title=lead[3]
        on(NewLeadPage).email=lead[4]
        on(NewLeadPage).mobile=lead[5]
        on(NewLeadPage).street=lead[6]
        on(NewLeadPage).city=lead[7]
        on(NewLeadPage).zip=lead[8]
        on(NewLeadPage).state=lead[9]
        on(NewLeadPage).save
    end
    
#     it "opens the lead's detail page" do
#         visit(LeadsPage)
#         lname = lead[0,2].join(" ")
#         on(LeadsPage).lead_anchor(lname).lead_link_element.click
#     end
    
#     it "creates a note for the lead" do
#         # note
#     end

#     it "provides means to add a lead" do
#         expect(NewLeadsPage.create_lead_element.when_visible.href).to eq "/leads/new"
#     end

#     it "lists leads" do
#        nlead = get_created_lead_from_list content
#        expect(nlead)
#     end
end


RSpec.describe "Leads" do
    before(:all) do
        login_to_autotest
        visit(NewLeadPage)
    end
    
    describe "Create lead and a note" do
        lead = generate_lead
        note = generate_note
        include_examples "Add a lead and a note", lead, note
    end
end

